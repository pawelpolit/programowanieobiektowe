package satori.java;

import org.junit.Test;
import satori.java.Functions;
import satori.java.Functions.GenericFunctionsException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * @author Pawel Polit
 */
public class FunctionsTest {

    public class StrRvs implements Functions.Function<String, String> {
        @Override
        public int arity() {
            return 1;
        }

        @Override
        public String compute(List<? extends String> args) throws GenericFunctionsException {
            if (args == null || args.size() != arity()) throw new GenericFunctionsException();
            return new StringBuilder(args.get(0)).reverse().toString();
        }
    }

    public class StrConcat implements Functions.Function<String, String> {
        @Override
        public int arity() {
            return 2;
        }

        @Override
        public String compute(List<? extends String> args) throws GenericFunctionsException {
            if (args == null || args.size() != arity()) throw new GenericFunctionsException();
            return args.get(0) + args.get(1);
        }
    }

    @Test
    public void Test1() throws GenericFunctionsException {
        Functions.Function<String, String> f = Functions.constant("Ala");
        assertEquals("Ala", f.compute(Collections.<String>emptyList()));
        assertEquals(Double.valueOf(123.0), Functions.constant(123.0).compute(Collections.<Double>emptyList()));
        boolean ok = false;
        try {
            Functions.constant("Ala").compute(Collections.singletonList("Ala"));
        } catch (GenericFunctionsException e) {
            ok = true;
        }
        assertTrue(ok);
    }

    @Test
    public void Test2() throws GenericFunctionsException {
        Functions.Function<Integer, Integer> f = Functions.proj(3, 0);
        assertEquals(Integer.valueOf(1), f.compute(Arrays.asList(1, 2, 3)));
        assertEquals("ma", Functions.<String, String>proj(2, 1).compute(Arrays.asList("Ala", "ma")));
        boolean ok = false;
        try {
            Functions.<String, String>proj(2, 1).compute(Collections.singletonList("Ala"));
        } catch (GenericFunctionsException e) {
            ok = true;
        }
        assertTrue(ok);
    }

    @Test
    public void Test3() throws GenericFunctionsException {
        Functions.Function<String, String> f = new StrRvs();
        List<String> ala = Collections.singletonList("Ala");
        assertEquals("alA", (f.compute(ala)));
        f = Functions.compose(f, Collections.singletonList(f));
        assertEquals("Ala", f.compute(ala));
        f = Functions.compose(new StrRvs(), Collections.singletonList(f));
        assertEquals("alA", f.compute(ala));
    }

    @Test
    public void Test4() throws GenericFunctionsException {
        Functions.Function<String, String> f = Functions.compose(new StrRvs(), Collections.singletonList(new StrRvs()));
        f = Functions.compose(new StrConcat(), Arrays.asList(f, new StrRvs()));
        assertEquals("AlaalA", f.compute(Collections.singletonList("Ala")));
        f = Functions.compose(new StrConcat(), Arrays.asList(new StrConcat(), new StrConcat()));
        assertEquals("****", f.compute(Arrays.asList("*", "*")));
    }

    @Test
    public void Test5() throws GenericFunctionsException {
        Functions.Function<String, String> f = Functions.compose(new StrRvs(), Collections.singletonList(new StrRvs()));
        f = Functions.compose(new StrConcat(), Arrays.asList(f, new StrRvs()));
        assertEquals("AlaalA", f.compute(Collections.singletonList("Ala")));
        assertEquals("ma", Functions.proj(3, 1).compute(Arrays.asList("Ala", "ma", "kota")));
        f = Functions.compose(new StrConcat(), Arrays.asList(Functions.<String, String>proj(2, 0), Functions.<String, String>proj(2, 1)));
        assertEquals("AlaMa", f.compute(Arrays.asList("Ala", "Ma")));
        f = Functions.compose(f, Arrays.asList(
                Functions.compose(f, Arrays.asList(
                        Functions.<String, String>proj(4, 0),
                        Functions.<String, String>proj(4, 1))),
                Functions.compose(f, Arrays.asList(
                        Functions.<String, String>proj(4, 2),
                        Functions.<String, String>proj(4, 3)))));
        assertEquals("AlaMaKota?", f.compute(Arrays.asList("Ala", "Ma", "Kota", "?")));
        f = Functions.compose(
                f,
                Arrays.asList(Functions.<String, String>proj(1, 0),
                        Functions.<String, String>proj(1, 0),
                        Functions.<String, String>proj(1, 0),
                        Functions.<String, String>proj(1, 0)));
        assertEquals("++++", f.compute(Collections.singletonList("+")));
    }

    @Test
    public void Test6() throws GenericFunctionsException {
        Functions.Function<Double, Double> plus = new Functions.Function<Double, Double>() {

            @Override
            public int arity() {
                return 2;
            }

            @Override
            public Double compute(List<? extends Double> args) throws GenericFunctionsException {
                if (args == null || args.size() != arity()) throw new GenericFunctionsException();
                return args.get(0) + args.get(1);
            }

        };
        Functions.Function<Double, Integer> plus1 = new Functions.Function<Double, Integer>() {

            @Override
            public int arity() {
                return 2;
            }

            @Override
            public Integer compute(List<? extends Double> args) throws GenericFunctionsException {
                if (args == null || args.size() != arity()) throw new GenericFunctionsException();
                return args.get(0).intValue() + args.get(1).intValue();
            }

        };
        Functions.Function<Number, Number> razy = new Functions.Function<Number, Number>() {
            @Override
            public int arity() {
                return 2;
            }

            @Override
            public Number compute(List<? extends Number> args) throws GenericFunctionsException {
                if (args == null || args.size() != arity()) throw new GenericFunctionsException();
                return args.get(0).doubleValue() * args.get(1).doubleValue();
            }
        };

        Double a = 16.0;
        List list = new ArrayList<Functions.Function<? extends Number, ? extends Number>>();
        list.add(plus);
        list.add(plus1);
        assertEquals(a, Functions.compose(razy, list).compute(Arrays.<Double>asList(2.0, 2.0)));
    }

    @Test
    public void Test7() throws GenericFunctionsException {
        Functions.Function<Integer, Integer> f = Functions.constant(2);
        List<Integer> args = new ArrayList<>();
        assertEquals(2, f.compute(args).intValue());
        List<Functions.Function<Integer, Integer>> arg = new ArrayList<>();
        f = Functions.compose(f, arg);
        assertEquals(2, f.compute(args).intValue());
    }
}