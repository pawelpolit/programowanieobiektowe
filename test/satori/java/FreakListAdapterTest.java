package satori.java;

import org.junit.Test;
import satori.java.FreakListAdapter;
import satori.java.FreakListAdapter.FreakList;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static satori.java.FreakListAdapter.*;

/**
 * @author Pawel Polit
 */
public class FreakListAdapterTest  {
    @Test
    public void testExample() throws Exception {
        FreakList<Integer> Cycle11 = new FreakList<Integer> () {
            private LinkedList<Integer> ttt = new LinkedList<>(); {
                int a = 2;
                for (int i = 0; i < 10; i++) {
                    ttt.add(a);
                    a = (2*a)%11;
                }
            }
            @Override
            public Integer pop() throws AccessDenied {
                if (ttt.isEmpty())
                    throw new IsEmpty();
                return ttt.pop();
            }
        };

        FreakListAdapter<Integer> f = new FreakListAdapter<>(Cycle11);

        List<Integer> expectedResults = Arrays.asList(2,4,8,5,10,9,7,3,6,1);

        for(Integer i : expectedResults) {
            try {
                assertEquals(i, f.pop());
            }
            catch (IsEmpty e) {
                fail();
            }
        }
        try {
            f.pop();
            fail();
        }
        catch (IsEmpty ignored) {}
    }
    
    
    
    
    
    
    @Test
    public void testWrongValues() throws Exception{
        FreakList<Integer> fl = new FreakList<Integer>() {
            int counter = 0;
            @Override
            public Integer pop() throws AccessDenied {
                switch(counter++) {
                    case 1:
                        throw new AccessDenied();
                    case 2:
                        throw new WasWrong();
                    case 3:
                        throw new AccessDenied();
                    case 4:
                        return 2;
                    case 5:
                        throw new AccessDenied();
                    case 6:
                        throw new WasWrong();
                    case 7:
                        throw new AccessDenied();
                    case 8:
                        return 1;
                    case 9:
                        throw new AccessDenied();
                    case 10:
                        return 3;
                    case 11:
                        throw new AccessDenied();
                    default:
                        throw new IsEmpty();
                }
            }
        };
        FreakListAdapter<Integer> f = new FreakListAdapter<>(fl);

        List<Integer> expectedResults = Arrays.asList(1,2,3);

        for(Integer i : expectedResults) {
            try {
                assertEquals(i, f.pop());
            }
            catch (IsEmpty e) {
                fail();
            }
        }
        try {
            f.pop();
            fail();
        }
        catch (IsEmpty ignored) {}
    }
}
