package satori.java;

import java.io.Reader;
import java.util.HashMap;
import java.util.Scanner;

/**
 * @author Pawel Polit
 */
public class Relay {
    private Scanner order;
    private HashMap<Integer, Thread> competitor;
    private Thread next = null;
    private boolean started = false;
    private boolean someoneRun = false;
    private boolean end = false;
    private Thread readNext;
    
    public Relay(Reader reader) {
        order = new Scanner(reader);
        competitor = new HashMap<>();
        readNext = new read();
    }
    
    public synchronized void register(int id, Thread competitor) {
        this.competitor.put(id, competitor);
    }
    
    public synchronized void startRelayRace() {
        started = true;
        readNext.start();
    }
    
    public boolean dispatch() {
        if(end || !competitor.containsValue(Thread.currentThread())) {
            return false;
        }
        
        if(started) {
            try  {
                if(!Thread.currentThread().equals(next)) {
                    if(next != null) {
                        synchronized(next) {
                            next.notify();
                        }
                    }
                    synchronized(Thread.currentThread()) {
                        Thread.currentThread().wait();
                    }
                }
                
            } 
            catch (InterruptedException e)  {
                e.printStackTrace();
            }
        }
        else {
            try  {
                synchronized(Thread.currentThread()) {
                    Thread.currentThread().wait();
                }
            } 
            catch (InterruptedException e)  {
                e.printStackTrace();
            }
        }
        
        if(end) {
            return false;
        }
        
        if(someoneRun) {
            someoneRun = false;
            synchronized(readNext) {
                readNext.notify();
            }
            
            synchronized(Thread.currentThread()) {
                try  {
                    Thread.currentThread().wait();
                } 
                catch (InterruptedException e)  {
                    e.printStackTrace();
                }
            }
        }
        
        if(end) {
            return false;
        }
        
        someoneRun = true;
        return true;
    }
    
    private class read extends Thread {

        @Override
        public void run() {
            while(order.hasNextInt()) {
                synchronized(this) {
                    next = competitor.get(order.nextInt());
                    synchronized(next) {
                        next.notify();
                    }
                    
                    try  {
                        this.wait();
                    } 
                    catch (InterruptedException e)  {
                        e.printStackTrace();
                    }
                }
            }
            
            end = true;
            
            for(Thread thread : competitor.values()) {
                synchronized(thread) {
                    thread.notify();
                }
            }
        }
    }
}
