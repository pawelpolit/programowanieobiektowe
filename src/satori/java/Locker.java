package satori.java;

import java.util.concurrent.ConcurrentHashMap;

/**
 * @author Pawel Polit
 */
public class Locker {

    public static class DeadlockException extends Exception{}

    private static ConcurrentHashMap<Thread, Lock> waiting = new ConcurrentHashMap<>();

    public class Lock{
        private int number;
        private Thread owner;

        public Lock(){
            number = 0;
            owner = null;
        }

        public synchronized void lock() throws InterruptedException, DeadlockException{
            if(number == 0){
                owner = Thread.currentThread();
                number++;
            }else if(Thread.currentThread().equals(owner)){
                number++;
            }else{
                Thread tmp = owner;
                while(tmp != null){
                    if(Thread.currentThread().equals(tmp)){
                        throw new DeadlockException();
                    }
                    Lock temp = waiting.get(tmp);
                    tmp = (temp == null) ? null : temp.owner;
                }

                waiting.put(Thread.currentThread(), this);
                this.wait();
                waiting.remove(Thread.currentThread());
                this.lock();
            }
        }

        public synchronized void unlock(){
            if(Thread.currentThread().equals(owner)){
                if(--number == 0){
                    owner = null;
                    this.notify();
                }
            }else{
                throw new IllegalMonitorStateException();
            }
        }
    }

    public Lock getLock(){
        return new Lock();
    }
}