package satori.java;

import org.junit.Test;
import satori.java.Polynomial;

import static org.junit.Assert.assertEquals;

/**
 * @author Pawel Polit
 */
public class PolynomialTest {

    @Test
    public void test1() {
        Polynomial p = new Polynomial();
        assertEquals("x", p.toString());
        p = Polynomial.multiply(p, p);
        assertEquals("x^2", p.toString());
        p = Polynomial.add(p, new Polynomial());
        assertEquals("x^2 + x", p.toString());
        p.shiftOy(5);
        assertEquals("x^2 + x + 5", p.toString());
        p.shiftOx(5);
        assertEquals("x^2 - 9x + 25", p.toString());
        p = Polynomial.multiply(p, new Polynomial(-1));
        assertEquals("-x^2 + 9x - 25 has value -11 at 2", p + " has value " + p.valueAt(2L) + " at 2");
        p = new Polynomial(0);
        assertEquals(" has value 0 at 2", p + " has value " + p.valueAt(2L) + " at 2");
        p = new Polynomial();
        p = Polynomial.compose(Polynomial.add(Polynomial.multiply(p, p), new Polynomial(3)), Polynomial.add(Polynomial.multiply(Polynomial.multiply(p, p),p), new Polynomial(-13)));
        assertEquals("x^6 - 26x^3 + 172", p.toString());
    }

    @Test
    public void test2() {
        Polynomial x = Polynomial.multiply(new Polynomial(), new Polynomial());
        assertEquals("x^2", x.toString());
        x = Polynomial.compose(x, Polynomial.add(x, new Polynomial(1)));
        assertEquals("x^4 + 2x^2 + 1", x.toString());
        x.shiftOy(-1);
        assertEquals("x^4 + 2x^2", x.toString());
        x.shiftOx(-1);
        assertEquals("x^4 + 4x^3 + 8x^2 + 8x + 3", x.toString());
        x.shiftOx(1);
        assertEquals("x^4 + 2x^2", x.toString());
        x = Polynomial.compose(x, new Polynomial(0));
        assertEquals("", x.toString());
    }

    @Test
    public void test3() {

        Polynomial w = new Polynomial();
        assertEquals("x", w.toString());
        w.shiftOx(5);
        assertEquals("x - 5", w.toString());
        w = Polynomial.multiply(w, new Polynomial());
        assertEquals("x^2 - 5x", w.toString());
        w = Polynomial.compose(w, w);
        assertEquals("x^4 - 10x^3 + 20x^2 + 25x", w.toString());
        w = Polynomial.add(w, new Polynomial(10));
        assertEquals("x^4 - 10x^3 + 20x^2 + 25x + 10", w.toString());
        w.shiftOy(-5);
        assertEquals("x^4 - 10x^3 + 20x^2 + 25x + 5", w.toString());
        
        Polynomial u = new Polynomial();
        assertEquals("x", u.toString());
        u = Polynomial.multiply(u, new Polynomial());
        assertEquals("x^2", u.toString());
        u = Polynomial.multiply(u, new Polynomial());
        assertEquals("x^3", u.toString());
        u = Polynomial.multiply(u, new Polynomial());
        assertEquals("x^4", u.toString());
        u = Polynomial.multiply(u, new Polynomial(-1));
        assertEquals("-x^4", u.toString());
        u = Polynomial.multiply(u, new Polynomial());
        assertEquals("-x^5", u.toString());
        
        u = Polynomial.add(u, w);
        assertEquals("-x^5 + x^4 - 10x^3 + 20x^2 + 25x + 5", u.toString());
        u.shiftOx(-5);
        assertEquals("-x^5 - 24x^4 - 240x^3 - 1230x^2 - 3150x - 3120", u.toString());
        u.shiftOx(5);
        assertEquals("-x^5 + x^4 - 10x^3 + 20x^2 + 25x + 5", u.toString());
        
        Polynomial a = Polynomial.multiply(Polynomial.multiply(new Polynomial(), new Polynomial()), Polynomial.multiply(new Polynomial(), new Polynomial()));
        assertEquals("x^4", a.toString());
        Polynomial b = Polynomial.add(a, new Polynomial(-1));
        assertEquals("x^4 - 1", b.toString());
        Polynomial c = Polynomial.add(a, new Polynomial(1));
        assertEquals("x^4 + 1", c.toString());
        Polynomial d = Polynomial.multiply(b, c);
        assertEquals("x^8 - 1", d.toString());
        d = Polynomial.multiply(d, new Polynomial(5));
        assertEquals("5x^8 - 5", d.toString());
        d = Polynomial.add(u, d);
        assertEquals("5x^8 - x^5 + x^4 - 10x^3 + 20x^2 + 25x", d.toString());
        d = Polynomial.add(d, Polynomial.multiply(u, new Polynomial(-1)));
        assertEquals("5x^8 - 5", d.toString());
        d.shiftOy(5);
        assertEquals("5x^8", d.toString());
    }
}
