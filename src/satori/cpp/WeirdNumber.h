#ifndef WEIRD_NUMBER_H
#define WEIRD_NUMBER_H

#include <iostream>
using namespace std;


class WeirdNumber{
    int a, b;
    WeirdNumber* increment;
    WeirdNumber* decrement;

public:
    WeirdNumber(int i, int j, WeirdNumber* in, WeirdNumber* de);
    WeirdNumber(const WeirdNumber& num);
    ~WeirdNumber();
    int getA() const;
    int getB() const;
    WeirdNumber& operator=(const WeirdNumber& num);
    WeirdNumber& operator+=(const WeirdNumber& num);
    WeirdNumber& operator-=(const WeirdNumber& num);
    WeirdNumber& operator*=(const WeirdNumber& num);
    WeirdNumber& operator++();
    WeirdNumber& operator--();
    WeirdNumber operator++(int);
    WeirdNumber operator--(int);
};

WeirdNumber::WeirdNumber(int i = 0, int j = 0, WeirdNumber* in = NULL, WeirdNumber* de = NULL): a(i), b(j), increment(in), decrement(de){}

WeirdNumber::WeirdNumber(const WeirdNumber& num): a(num.getA()), b(num.getB()), increment(NULL), decrement(NULL){}

WeirdNumber::~WeirdNumber(){
   if(increment != NULL){
        ++(*increment);
   }
   if(decrement != NULL){
        --(*decrement);
   }
}

int WeirdNumber::getA() const{
    return a;
}

int WeirdNumber::getB() const{
    return b;
}

WeirdNumber& WeirdNumber::operator=(const WeirdNumber& num){
    if(&num != this){
        a = num.a;
        b = num.b;
    }
    return *this;
}

WeirdNumber& WeirdNumber::operator+=(const WeirdNumber& num){
    a += num.getA();
    b += num.getB();
    return *this;
}

WeirdNumber& WeirdNumber::operator-=(const WeirdNumber& num){
    a -= num.getA();
    b -= num.getB();
    return *this;
}

WeirdNumber& WeirdNumber::operator*=(const WeirdNumber& num){
    int tempA = getA() * num.getA() + 2 * getB() * num.getB();
    int tempB = getA() * num.getB() + getB() * num.getA();
    a = tempA;
    b = tempB;
    return *this;
}

WeirdNumber& WeirdNumber::operator++(){
    a += 1;
    return *this;
}

WeirdNumber& WeirdNumber::operator--(){
    a -= 1;
    return *this;
}

WeirdNumber WeirdNumber::operator++(int){
    return WeirdNumber(a, b, this);
}

WeirdNumber WeirdNumber::operator--(int){
    return WeirdNumber(a, b, NULL, this);
}


WeirdNumber operator+(const WeirdNumber& first, const WeirdNumber& second){
    return WeirdNumber(first.getA() + second.getA(), first.getB() + second.getB());
}

WeirdNumber operator-(const WeirdNumber& first, const WeirdNumber& second){
    return WeirdNumber(first.getA() - second.getA(), first.getB() - second.getB());
}

WeirdNumber operator*(const WeirdNumber& first, const WeirdNumber& second){
    return WeirdNumber(first.getA() * second.getA() + 2 * first.getB() * second.getB(), first.getA() * second.getB() + first.getB() * second.getA());
}

bool operator==(const WeirdNumber& first, const WeirdNumber& second){
    return (first.getA() == second.getA() && first.getB() == second.getB());
}

bool operator!=(const WeirdNumber& first, const WeirdNumber& second){
    return !(first == second);
}

ostream& operator<<(ostream& out, const WeirdNumber& num){
    if(num.getA() == 0 && num.getB() == 0){
        out << 0;
    }else if(num.getB() == 0){
        out << num.getA();
    }else if(num.getA() == 0){
        if(num.getB() == 1){
            out << "s";
        }else if(num.getB() == -1){
            out << "-s";
        }else{
            out << num.getB() << "s";
        }
    }else{
        out << num.getA();
        if(num.getB() == 1){
            out << "+s";
        }else if(num.getB() == -1){
            out << "-s";
        }else if(num.getB() > 0){
            out << "+" << num.getB() << "s";
        }else{
            out << num.getB() << "s";
        }
    }
    return out;
}

#endif
