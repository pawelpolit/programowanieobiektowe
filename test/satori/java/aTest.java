package satori.java;

import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;

/**
 * @author Pawel Polit
 */
public class aTest {
    @Test
    public void test0() {
        a a = new a();
        a.a(1);
        a.a(2);
        a.a(3);
        assertEquals(String.valueOf(a), "[1, 2, 3]");
    }
    
    @Test
    public void test1()  {
        a a = new a();
        a.a(1);
        a.a(3);
        a.a(2);
        assertEquals(String.valueOf(a), "[1, 2, 3]");
    }
    
    @Test
    public void test2()  {
        a a = new a();
        a.a(1).a(3).a(2);
        assertEquals(String.valueOf(a), "[1, 2, 3]");
        assertEquals(String.valueOf(a.a), "[1, 3, 2]");
    }
    
    @Test
    public void test3()  {
        a a = new a();
        a.a(1).a(5).a(2).a(4).a(3);
        assertEquals(String.valueOf(a), "[1, 2, 3, 4, 5]");
        assertEquals(String.valueOf(a.a), "[1, 5, 2, 4, 3]");
        assertEquals(String.valueOf(a.a()), "3");
        assertEquals(String.valueOf(a), "[1, 2, 4, 5]");
        assertEquals(String.valueOf(a.a), "[1, 5, 2, 4]");
        assertEquals(String.valueOf(a.a()), "4");
        assertEquals(String.valueOf(a), "[1, 2, 5]");
        assertEquals(String.valueOf(a.a), "[1, 5, 2]");
        assertEquals(String.valueOf(a.a()), "2");
        assertEquals(String.valueOf(a), "[1, 5]");
        assertEquals(String.valueOf(a.a), "[1, 5]");
        assertEquals(String.valueOf(a.a()), "5");
        assertEquals(String.valueOf(a), "[1]");
        assertEquals(String.valueOf(a.a), "[1]");
        assertEquals(String.valueOf(a.a()), "1");
        assertEquals(String.valueOf(a), "[]");
        assertEquals(String.valueOf(a.a), "[]");
    }
    
    @Test
    public void test4()  {
        a a = new a();
        a.a(1).a(5).a(2).a(4).a(3);
        assertEquals(String.valueOf(a), "[1, 2, 3, 4, 5]");
        assertEquals(String.valueOf(a.a), "[1, 5, 2, 4, 3]");
        a.a(a.a());
        assertEquals(String.valueOf(a), "[1, 2, 3, 4, 5]");
        assertEquals(String.valueOf(a.a), "[3, 1, 5, 2, 4]");
        a.a(a.a());
        assertEquals(String.valueOf(a), "[1, 2, 3, 4, 5]");
        assertEquals(String.valueOf(a.a), "[4, 3, 1, 5, 2]");
        a.a(a.a()).a(a.a()).a(a.a());
        assertEquals(String.valueOf(a), "[1, 2, 3, 4, 5]");
        assertEquals(String.valueOf(a.a), "[1, 5, 2, 4, 3]");
        a.a(1).a(5);
        assertEquals(String.valueOf(a), "[1, 1, 2, 3, 4, 5, 5]");
        assertEquals(String.valueOf(a.a), "[1, 5, 2, 4, 3, 1, 5]");
        a.a(a.a());
        assertEquals(String.valueOf(a), "[1, 1, 2, 3, 4, 5, 5]");
        assertEquals(String.valueOf(a.a), "[5, 1, 5, 2, 4, 3, 1]");
        a.a(a.a()).a(a.a()).a(a.a());
        assertEquals(String.valueOf(a), "[1, 1, 2, 3, 4, 5, 5]");
        assertEquals(String.valueOf(a.a), "[4, 3, 1, 5, 1, 5, 2]");
    }
    
    @Test
    public void test5()  {
        a a = new a();
        a.a(1).a(1).a(2).a(1).a(1);
        assertEquals(String.valueOf(a.a), "[1, 1, 2, 1, 1]");
        a.a.a(3).a.a(1).a(1).a.a.a(4);
        assertEquals(String.valueOf(a.a), "[1, 1, 2, 1, 1, 3, 1, 1, 4]");
        a = a.a;
        a.a(1).a(1).a.a.a(5);
        assertEquals(String.valueOf(a), "[1, 1, 2, 1, 1, 3, 1, 1, 4, 1, 1, 5]");
    }
    
    @Test
    public void test6()  {
        a a = new a();
        a.a(1).a(5).a(2).a(4).a(3);
        assertEquals(Arrays.toString(new int[] { a.a(), a.a(), a.a(), a.a(), a.a() }), "[3, 4, 2, 5, 1]");
    }
    
    @Test
    public void test7()  {
        a a = new a();
        a.a.a.a.a.a.a.a.a.a(1);
        a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a(3);
        a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a(2);
        assertEquals(String.valueOf(a), "[1, 2, 3]");
    }
    
    @Test
    public void test8()  {
        a a = new a();
        a.a(1).a(3).a(2);
        assertEquals(String.valueOf(a), "[1, 2, 3]");
        assertEquals(String.valueOf(a.a), "[1, 3, 2]");
        assertEquals(String.valueOf(a.a.a), "[1, 2, 3]");
        assertEquals(String.valueOf(a.a.a.a), "[1, 3, 2]");
        assertEquals(String.valueOf(a.a.a.a.a), "[1, 2, 3]");
        for (int i = 0; i < 1000000; i++)  {
            assertEquals(String.valueOf(a), "[1, 2, 3]");
            a = a.a;
            assertEquals(String.valueOf(a), "[1, 3, 2]");
            a = a.a;
        }
    }
    
    @Test
    public void test9()  {
        a a = new a();
        assertEquals(String.valueOf(a.a(1).a(3).a(2)),"[1, 2, 3]");
        a = new a();
        assertEquals(String.valueOf(a.a(1).a(3).a(2).a),"[1, 3, 2]");
        a = new a();
        assertEquals(String.valueOf(a.a.a(1).a(3).a(2)),"[1, 2, 3]");
        a = new a();
        assertEquals(String.valueOf(a.a.a(1).a.a.a(3).a.a.a.a.a(2)),"[1, 2, 3]");
    }
    
    @Test
    public void test10()  {
        a a = new a();
        assertEquals(String.valueOf(a.a(1).a(2).a(3).a(4).a(5)),"[1, 2, 3, 4, 5]");
        assertEquals(String.valueOf(a.a(a.a())),"[5, 1, 2, 3, 4]");
        assertEquals(String.valueOf(a.a(a.a())),"[4, 5, 1, 2, 3]");
        assertEquals(String.valueOf(a.a(6)),"[1, 2, 3, 4, 5, 6]");
        assertEquals(String.valueOf(a.a(a.a())),"[6, 4, 5, 1, 2, 3]");
    }
    
    @Test
    public void test11()  {
        String[] one = {"1", "3", "2"};
        String[] two = {"1", "2", "3"};
        a o = new a();
        int i = 0;
        o.a(1);
        o.a(3);
        o.a(2);
        for (a a : o)
            assertEquals(String.valueOf(a), one[i++]);
        i = 0;
        for (a a : o.a)
            assertEquals(String.valueOf(a), two[i++]);
    }
}
