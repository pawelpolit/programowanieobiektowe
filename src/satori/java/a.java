package satori.java;

import java.util.Iterator;

/**
 * @author Pawel Polit
 */
public class a implements Iterable<a> {
    private class element {
        private int number;
        public element next;
        
        public element(int n, element x) {
            number = n;
            next = x;
        }
    }
    
    private class listIterator implements Iterator<a> {
        private element temp = a.getHead().next;

        @Override
        public boolean hasNext() {
            return (temp != null);
        }

        @Override
        public a next() {
            a tmp = new a(temp.number);
            temp = temp.next;
            return tmp;
        }

        @Override
        public void remove(){}
    }

    @Override
    public Iterator<a> iterator() {
        return new listIterator();
    }
    
    public class unsorted extends a implements Iterable<a> {
        private element head;
        private element last;
        public a a;

        public unsorted(a sorted) {
            super(sorted);
            head = new element(0, null);
            last = head;
            a = sorted;
        }

        @Override
        public a a(int n) {
            return a.a(n);
        }

        @Override
        public a a(Integer n) {
            return a.a(n);
        }

        @Override
        public element getHead() {
            return head;
        }

        @Override
        public void add(int n) {
            last.next = new element(n, null);
            last = last.next;
        }

        @Override
        public void addFirst(int n) {
            head.next = new element(n, head.next);
        }

        @Override
        public Integer pop() {
            if(head.next == null) {
                return 0;
            }
            Integer res = last.number;
            element tmp = head;
            while(tmp.next != last) {
                tmp = tmp.next;
            }
            tmp.next = null;
            last = tmp;
            return res;
        }

        private class listIterator implements Iterator<a> {
            private element temp = a.head.next;

            @Override
            public boolean hasNext() {
                return (temp != null);
            }

            @Override
            public a next() {
                a tmp = new a(temp.number);
                temp = temp.next;
                return tmp;
            }

            @Override
            public void remove(){}
        }

        @Override
        public Iterator<a> iterator() {
            return new listIterator();
        }

        @Override
        public String toString() {
            StringBuilder res = new StringBuilder();
            if(head.next == null) {
                res.append("[]");
            }
            else {
                res.append('[');
                element tmp = head;
                while(tmp.next != last) {
                    tmp = tmp.next;
                    res.append(tmp.number);
                    res.append(", ");
                }
                res.append(last.number);
                res.append(']');
            }
            return res.toString();
        }
    }
    
    private element head;
    private element last;
    public a a;
    
    public a() {
        head = new element(0, null);
        last = head;
        a = new unsorted(this);
    }
    
    public a(a tmp) {
        a = tmp;
    }
    
    public a(int n) {
        head = new element(n, null);
        last = null;
        a = null;
    }
    
    public a a(int n) {
        element tmp = head;
        while(tmp.next != null && tmp.next.number < n) {
            tmp = tmp.next;
        }
        tmp.next = new element(n, tmp.next);
        if(last.next != null) {
            last = last.next;
        }
        a.add(n);
        return this;
    }
    
    public a a(Integer i) {
        int n = i;
        element tmp = head;
        while(tmp.next != null && tmp.next.number < n) {
            tmp = tmp.next;
        }
        tmp.next = new element(n, tmp.next);
        if(last.next != null) {
            last = last.next;
        }
        a.addFirst(n);
        return this.a;
    }
    
    public Integer a() {
        if(a == null) {
            return 0;
        }
        Integer n = a.pop();
        if(head.next != null) {
            element tmp = head;
            while(n.compareTo(tmp.next.number) != 0) {
                tmp = tmp.next;
            }
            tmp.next = tmp.next.next;
            if(tmp.next == null) {
                last = tmp;
            }
        }
        return n;
    }

    @Override
    public String toString() {
        if(last == null) {
            return String.valueOf(head.number);
        }
        StringBuilder res = new StringBuilder();
        if(head.next == null) {
            res.append("[]");
        }
        else {
            res.append('[');
            element tmp = head;
            while(tmp.next != last) {
                tmp = tmp.next;
                res.append(tmp.number);
                res.append(", ");
            }
            res.append(last.number);
            res.append(']');
        }
        return res.toString();
    }
    
    public void add(int n){}
    
    public void addFirst(int n){}
    
    public Integer pop() {
        return 0;
    }
    
    public element getHead() {
        return head;
    }
}
