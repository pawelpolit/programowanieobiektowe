package satori.java;

/**
 * @author Pawel Polit
 */
public class FreakListAdapter<T> {
    private T prev;
    private boolean wasEmpty;
    private boolean wasWrong;
    private FreakList<T> list;
    
    
    public FreakListAdapter(FreakList<T> fl) {
        list = fl;
        wasEmpty = false;
        wasWrong = false;
        prev = null;
    }
    
    public T pop() throws IsEmpty {
        T element = null;
        AccessDenied acc;
        do {
            acc = null;
            try {
                element = list.pop();
            }
            catch(IsEmpty e) {
                if(wasEmpty && prev == null) {
                    throw(e);
                }
                else if(wasEmpty) {
                    wasEmpty = false;
                    element = prev;
                    prev = null;
                    return element;
                }
                wasEmpty = true;
                return this.pop();
                
            }
            catch(WasWrong e) {
                if(wasEmpty) {
                    wasEmpty = false;
                    return pop();
                }
                else {
                    wasWrong = true;
                    return pop();
                }	
            }
            catch(AccessDenied e) {
                acc = e;
            }
        } while(acc != null);
        
        if(prev == null) {
            prev = element;
            return pop();
        }
        if(wasWrong) {
            wasWrong = false;
            return element;
        }
        T c = prev;
        prev = element;
        element = c;
        return element;
    }

    public interface FreakList <T> {
        T pop() throws AccessDenied;
    }

    public static class AccessDenied extends Throwable{}
    public static class WasWrong extends AccessDenied{}
    public static class IsEmpty extends WasWrong{}
}