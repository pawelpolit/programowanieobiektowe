package satori.java;

import java.math.BigInteger;

/**
 * @author Pawel Polit
 */
public class Polynomial  {
    private static class Monomial {
        private int deg;
        private BigInteger num;
        private Monomial next;
        
        public Monomial(BigInteger a, int b) {
            num = a;
            deg = b;
            next = null;
        }
        
        public Monomial(Monomial w) {
            deg = w.deg;
            num = w.num;
            next = null;
        }
    }
    
    private Monomial head;
    
    public Polynomial() {
        head = new Monomial(new BigInteger("0"), 0);
        head.next = new Monomial(new BigInteger("1"), 1);
    }
    
    public Polynomial(int c) {
        head = new Monomial(new BigInteger("0"), 0);
        head.next = new Monomial(BigInteger.valueOf(c), 0);
    }
    
    private Polynomial(Polynomial w) {
        head = new Monomial(new BigInteger("0"), 0);
        Monomial tmp1 = head;
        Monomial tmp2 = w.head;
        while(tmp2.next != null) {
            tmp2 = tmp2.next;
            tmp1.next = new Monomial(tmp2.num, tmp2.deg);
            tmp1 = tmp1.next;
        }
    }
    
    public BigInteger valueAt(long in) {
        BigInteger res = new BigInteger("0");
        Monomial tmp = head;
        while(tmp.next != null) {
            tmp = tmp.next;
            res = res.add(tmp.num.multiply(new BigInteger(String.valueOf(in)).pow(tmp.deg)));
        }
        return res;
    }
    
    public static Polynomial add(Polynomial w, Polynomial v) {
        Polynomial res = new Polynomial(w);
        Monomial tmp1 = v.head;
        Monomial tmp2 = res.head;
        while(tmp1.next != null) {
            tmp1 = tmp1.next;
            while(tmp2.next != null && tmp2.next.deg > tmp1.deg) {
                tmp2 = tmp2.next;				
            }
            if(tmp2.next == null || tmp2.next.deg < tmp1.deg) {
                if(tmp1.num.compareTo(BigInteger.valueOf(0)) != 0) {
                    Monomial a = new Monomial(tmp1);
                    a.next = tmp2.next;
                    tmp2.next = a;
                    tmp2 = tmp2.next;
                    if(tmp2.next != null && tmp2.next.deg == 0 && tmp2.next.num.compareTo(BigInteger.valueOf(0)) == 0) {
                        tmp2.next = null;
                    }
                }
            }
            else {
                tmp2.next.num = tmp2.next.num.add(tmp1.num);
                if(tmp2.next.num.compareTo(BigInteger.valueOf(0)) == 0) {
                    if(res.head == tmp2 && tmp2.next == null) {
                        tmp2.next.deg = 0;
                    }
                    else {
                        tmp2.next = tmp2.next.next;
                    }
                }
            }
        }
        return res;
    }
    
    public static Polynomial multiply(Polynomial w, Polynomial v) {
        if((w.head.next.deg == 0 && w.head.next.num.compareTo(BigInteger.valueOf(0)) == 0) || (v.head.next.deg == 0 && v.head.next.num.compareTo(BigInteger.valueOf(0)) == 0)) {
            return new Polynomial(0);
        }
        Polynomial res = new Polynomial(0);
        Monomial tmp1 = v.head;
        while(tmp1.next != null) {
            tmp1 = tmp1.next;
            Polynomial tmp = new Polynomial(w);
            Monomial tmp2 = tmp.head;
            while(tmp2.next != null) {
                tmp2 = tmp2.next;
                tmp2.deg += tmp1.deg;
                tmp2.num = tmp2.num.multiply(tmp1.num);
            }
            res = Polynomial.add(res, tmp);
        }
        return res;
    }
    
    public static Polynomial compose(Polynomial w, Polynomial v) {
        if(w.head.next.deg == 0) {
            return w;
        }
        else if(v.head.next.deg == 0) {
            return new Polynomial(w.valueAt(v.head.next.num.longValue()).intValue());
        }
        else {
            Polynomial t = new Polynomial(w);
            Monomial tmp0 = null;
            Monomial tmp1 = t.head.next;
            Monomial tmp2 = tmp1.next;
            while(tmp2 != null) {
                tmp1.next = tmp0;
                tmp0 = tmp1;
                tmp1 = tmp2;
                tmp2 = tmp2.next;
            }
            tmp1.next = tmp0;
            Polynomial pat = new Polynomial(1);
            Polynomial res = new Polynomial(0);
            long n = 0;
            while(tmp1 != null) {
                while(n < tmp1.deg) {
                    n++;
                    pat = Polynomial.multiply(pat, v);
                }
                Polynomial u = new Polynomial(pat);
                tmp0 = u.head;
                while(tmp0.next != null) {
                    tmp0 = tmp0.next;
                    tmp0.num = tmp0.num.multiply(tmp1.num);
                }
                res = Polynomial.add(res, u);
                tmp1 = tmp1.next;
            }
            return res;
        }
    }
    
    public void shiftOx(int move) {
        if(move != 0 && head.next.deg != 0) {
            Polynomial pat = Polynomial.add(new Polynomial(), new Polynomial(-move));
            Polynomial res = Polynomial.compose(this, pat);
            head.next = res.head.next;
        }
    }
    
    public void shiftOy(int move) {
        if(move != 0) {
            Monomial tmp = head;
            while(tmp.next.next != null) {
                tmp = tmp.next;
            }
            if(tmp.next.deg == 0) {
                tmp.next.num = tmp.next.num.add(BigInteger.valueOf(move));
                if(tmp.next.num.compareTo(BigInteger.valueOf(0)) == 0) {
                    tmp.next = null;
                }
            }
            else {
                tmp = tmp.next;
                tmp.next = new Monomial(BigInteger.valueOf(move), 0);
            }
        }
    }

    @Override
    public String toString() {
        StringBuilder res = new StringBuilder();
        Monomial tmp = head;
        while(tmp.next != null) {
            tmp = tmp.next;
            if(head.next == tmp) {
                if(tmp.num.compareTo(BigInteger.valueOf(1)) != 0) {
                    if(tmp.deg != 0 && tmp.num.compareTo(BigInteger.valueOf(-1)) == 0) {
                        res.append("-");
                    }
                    else if(tmp.deg != 0 || tmp.num.compareTo(BigInteger.valueOf(0)) != 0) {
                        res.append(tmp.num.toString());
                    }
                }
                else {
                    if(tmp.deg == 0) {
                        res.append(tmp.num.toString());
                    }
                }
            }
            else {
                res.append(' ');
                if(tmp.num.compareTo(BigInteger.valueOf(0)) == 1) {
                    res.append("+ ");
                    if(tmp.deg == 0 || tmp.num.compareTo(BigInteger.valueOf(1)) != 0) {
                        res.append(tmp.num.toString());
                    }
                }
                else {
                    res.append("- ");
                    if(tmp.deg == 0 || tmp.num.compareTo(BigInteger.valueOf(-1)) != 0) {
                        res.append(tmp.num.abs().toString());
                    }
                }
            }
            if(tmp.deg != 0) {
                res.append('x');
            }
            if(tmp.deg > 1) {
                res.append('^');
                res.append(tmp.deg);
            }
        }
        return res.toString();
    }
}