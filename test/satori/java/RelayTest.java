package satori.java;

import org.junit.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * @author Pawel Polit
 */
public class RelayTest {

    private static class TestClass extends Thread{
        private final int id;
        private final Relay myRelay;
        private static List<Integer> result = new ArrayList<>();

        public TestClass(int id, Relay myRelay) {
            this.id = id;
            this.myRelay = myRelay;
        }

        public void run() {
            while(myRelay.dispatch())  {
                result.add(id);
                synchronized(this) {
                    try {
                        wait(10);
                    } catch (InterruptedException ignored) {}
                }
            }
        }
    }

    @Test
    public void test() throws FileNotFoundException, InterruptedException {
        List<Integer> expectedResult = new ArrayList<>();
        expectedResult.add(0);
        expectedResult.add(1);
        expectedResult.add(0);
        expectedResult.add(2);
        expectedResult.add(1);
        expectedResult.add(2);
        expectedResult.add(1);
        expectedResult.add(0);
        expectedResult.add(2);
        expectedResult.add(1);
        expectedResult.add(0);
        expectedResult.add(0);

        Relay r = new Relay(new FileReader(new File("test/satori/java/RelayTestFile.txt")));
        TestClass[] t = {new TestClass(0,r), new TestClass(1,r), new TestClass(2,r)};
        for(int i = 0; i<3; i++ )  {
            r.register( i, t[i] );
            t[i].start();
        }
        r.startRelayRace();
        Thread.sleep(200);

        assertEquals(expectedResult, TestClass.result);

        TestClass.result.clear();
    }
}
