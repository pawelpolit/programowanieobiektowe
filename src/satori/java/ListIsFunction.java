package satori.java;

import java.util.*;

/**
 * @author Pawel Polit
 */
public class ListIsFunction  {
    private List list;
    private Map map;

    public ListIsFunction() {
        list = new ArrayList();
        
        map = new AbstractMap() {

            @Override
            public void clear() {
                list.clear();
            }

            @Override
            public boolean containsKey(Object key) {
                if(!(key instanceof Integer)) {
                    return false;
                }
                return ((int)key >= 0 && (int)key < list.size());
            }

            @Override
            public boolean containsValue(Object value) {
                return list.contains(value);
            }

            @Override
            public Set<Map.Entry<Integer, Object>> entrySet() {
                return new AbstractSet<Map.Entry<Integer, Object>>() {

                    @Override
                    public int size() {
                        return list.size();
                    }

                    @Override
                    public Iterator<Map.Entry<Integer, Object>> iterator() {
                        return new Iterator<Map.Entry<Integer, Object>>() {
                            Iterator it = list.iterator();
                            int prev = -1;

                            @Override
                            public boolean hasNext() {
                                return prev < list.size() - 1;
                            }

                            @Override
                            public Map.Entry<Integer, Object> next() {
                                prev++;
                                Object tmp = it.next();
                                return new AbstractMap.SimpleEntry<Integer, Object>(prev, tmp) {
                                    @SuppressWarnings("unchecked")
                                    @Override
                                    public Object setValue(Object o) {
                                        Object res = list.get(this.getKey());
                                        list.set(this.getKey(), o);
                                        super.setValue(o);
                                        return res;
                                    }
                                };
                            }

                            @Override
                            public void remove() {
                                map.remove(prev);
                            }
                        };
                    }

                    @Override
                    public String toString() {
                        StringBuilder res = new StringBuilder();
                        res.append('[');
                        if(list.size() == 0) {
                            res.append(']');
                            return res.toString();
                        }
                        int i;
                        for(i = 0; i < list.size() - 1; i++) {
                            res.append(i).append('=').append(list.get(i)).append(", ");
                        }
                        res.append(i).append('=').append(list.get(i)).append(']');
                        return res.toString();
                    }
                };	
            }

            @Override
            public boolean equals(Object o) {
                if(!(o instanceof Map)) {
                    return false;
                }
                return this.entrySet().equals(((Map)o).entrySet());
            }

            @Override
            public Object get(Object key) {
                if(key == null) {
                    throw new NullPointerException();
                }
                if(!this.containsKey(key)) {
                    return null;
                }
                return list.get((int)key);
            }

            @Override
            public int hashCode() {
                Set<Map.Entry<Integer, Object>> temp = this.entrySet();
                int res = 0;
                for(Object i : temp) {
                        res += i.hashCode();
                }
                return res;
            }

            @Override
            public boolean isEmpty() {
                return list.isEmpty();
            }

            @Override
            public Set keySet() {
                return new AbstractSet() {

                    @Override
                    public int size() {
                        return list.size();
                    }

                    @Override
                    public Iterator iterator() {
                        return new Iterator() {
                            int prev = -1;

                            @Override
                            public boolean hasNext() {
                                return prev < list.size() - 1;
                            }

                            @Override
                            public Object next() {
                                if(!this.hasNext()) {
                                    throw new NoSuchElementException();
                                }
                                prev++;
                                return prev;
                            }

                            @Override
                            public void remove() {
                                if(prev != list.size() - 1) {
                                    throw new IllegalStateException();
                                }
                                list.remove(prev);
                            }
                        };
                    }

                    @Override
                    public String toString() {
                        StringBuilder res = new StringBuilder();
                        res.append('[');
                        if(list.size() == 0) {
                            res.append(']');
                            return res.toString();
                        }
                        int i;
                        for(i = 0; i < list.size() - 1; i++) {
                            res.append(i).append(", ");
                        }
                        res.append(i).append(']');
                        return res.toString();
                    }
                };
            }
            
            @SuppressWarnings("unchecked")
            @Override
            public Object put(Object key, Object value) {
                if(key == null) {
                    throw new NullPointerException();
                }
                if(!(key instanceof Integer)) {
                    throw new ClassCastException();
                }
                if((int)key < 0 || (int)key > list.size()) {
                    throw new IllegalArgumentException();
                }
                if((int)key == list.size()) {
                    list.add(value);
                    return null;
                }
                return list.set((int)key, value);
            }

            @Override
            public void putAll(Map m) {
                Set keys = m.keySet();
                for(Object i : keys) {
                    this.put(i, m.get(i));
                }
            }

            @Override
            public Object remove(Object key) {
                if(key == null) {
                    throw new NullPointerException();
                }
                if(!(key instanceof Integer)) {
                    throw new ClassCastException();
                }
                if((int)key < 0 || (int)key >= list.size()) {
                    return null;
                }
                if((int)key != list.size() - 1) {
                    throw new IllegalArgumentException();
                }
                return list.remove((int)key);
            }

            @Override
            public int size() {
                return list.size();
            }

            @Override
            public String toString() {
                StringBuilder res = new StringBuilder();
                res.append('{');
                if(list.size() == 0) {
                    res.append('}');
                    return res.toString();
                }
                int i;
                for(i = 0; i < list.size() - 1; i++) {
                    res.append(i).append('=').append(list.get(i)).append(", ");
                }
                res.append(i).append('=').append(list.get(i)).append('}');
                return res.toString();
            }

            @Override
            public Collection values() {
                return list;
            }
        };
    }

    public List asList() {
        return list;
    }

    public Map asMap() {
        return map;
    }
}
