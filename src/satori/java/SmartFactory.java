package satori.java;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * @author Pawel Polit
 */
public class SmartFactory  {

    public static class HellNoException extends RuntimeException{}

    @SuppressWarnings("unchecked")
    public static <T> T fixIt(Class<T> cl, final Object obj) {
        
        return (T) Proxy.newProxyInstance(cl.getClassLoader(), new Class[]{cl}, (proxy, metoda, args) -> {
            if(obj == null) {
                throw new HellNoException();
            }

            Method[] metody = obj.getClass().getMethods();

            for(Method m : metody) {
                if(m.getName().equals(metoda.getName()) && metoda.getReturnType().isAssignableFrom(m.getReturnType()) && m.getParameterTypes().length == metoda.getParameterTypes().length) {
                    int n = m.getParameterTypes().length;

                    Class [] parametry1 = m.getParameterTypes();
                    Class [] parametry2 = metoda.getParameterTypes();

                    boolean dalej = false;

                    for(int i = 0; i < n; i++) {
                        if(!parametry1[i].isAssignableFrom(parametry2[i])) {
                            dalej = true;
                        }
                    }

                    if(dalej) {
                        continue;
                    }

                    Class [] wyjatki1 = m.getExceptionTypes();
                    Class [] wyjatki2 = metoda.getExceptionTypes();

                    for(Class w1 : wyjatki1) {
                        boolean ok = false;

                        for(Class w2 : wyjatki2) {
                            if(w2.isAssignableFrom(w1)) {
                                ok = true;
                            }
                        }

                        dalej = !ok;
                    }

                    if(dalej) {
                        continue;
                    }

                    Object res;

                    try {
                        res = m.invoke(obj, args);
                    }
                    catch(InvocationTargetException e) {
                        throw e.getTargetException();
                    }
                    catch(Exception e) {
                        continue;
                    }

                    return res;
                }
            }

            throw new HellNoException();
        });
    }	
}