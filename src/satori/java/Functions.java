package satori.java;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Pawel Polit
 */
public class Functions {
    public static <S, T> Function<S, T> constant(final T cons) {
        return new Function<S, T>() {
                    T res = cons;
                    
                    @Override
                    public int arity() {
                        return 0;
                    }

                    @Override
                    public T compute(List<? extends S> args) throws GenericFunctionsException {
                        if(args == null || args.size() != 0) {
                            throw new GenericFunctionsException();
                        }
                        return res;
                    }
                };
    }
    
    public static <S, T> Function<S, S> proj(final int n, final int k) throws GenericFunctionsException {
        if(n < 0 || k < 0 || k >= n) {
            throw new GenericFunctionsException();
        }
        return new Function<S, S>() {
                    int ar = n;
                    int at = k;
            
                    @Override
                    public int arity()  {
                        return ar;
                    }

                    @Override
                    public S compute(List<? extends S> args) throws GenericFunctionsException {
                        if(args == null || args.size() != ar) {
                            throw new GenericFunctionsException();
                        }
                        return args.get(at);
                    }
            
                };
    }
    
    public static <S, T, U> Function<S, U> compose(final Function<? super T, ? extends U> ext, final List<? extends Function<? super S, ? extends T>> in) throws GenericFunctionsException {
        if(ext == null || in == null || ext.arity() != in.size()) {
            throw new GenericFunctionsException();
        }
        int tmp1 = 0;
        if(ext.arity() > 0) {
            tmp1 = in.get(0).arity();
            for(int i = 1; i < in.size(); i++) {
                if(in.get(i) == null || in.get(i).arity() != tmp1) {
                    throw new GenericFunctionsException();
                }
            }
        }
        final int tmp = tmp1;
        return new Function<S, U>() {
                    int ar = tmp;
                    Function<? super T, ? extends U> external = ext;
                    List<? extends Function<? super S, ? extends T>> internal = in;

                    @Override
                    public int arity() {
                        return ar;
                    }

                    @Override
                    public U compute(List<? extends S> args) throws GenericFunctionsException {
                        if(args == null || args.size() != ar) {
                            throw new GenericFunctionsException();
                        }
                        List<T> arg = new ArrayList<>();
                        for (Function<? super S, ? extends T> function : internal) {
                            arg.add(function.compute(args));
                        }
                        return external.compute(arg);
                    }
                };
    }

    public static class GenericFunctionsException extends Exception {
    }

    public interface Function<T, S> {
        int arity();

        S compute(List<? extends T> args) throws GenericFunctionsException;
    }
}
