#include <iostream>
using namespace std;


class tracer{
    string* tab;
    mutable int printNumber;
    int size;

public:
    static int objectNumber;

    tracer();
    tracer(string const * const t, int s);
    tracer(const tracer& t);
    ~tracer();
    void concat(int k, int l);
    int getSize() const;
    string& operator[](int i) const;
    void print() const;
    int printno() const;
    static int objectno();
    tracer& operator=(const tracer& t);
};


int tracer::objectNumber = 0;

tracer::tracer(): size(0), printNumber(0){
    tab = new string[1];
    tab[0] = "";
    objectNumber++;
}

tracer::tracer(const string* t, int s): size(s), printNumber(0){
    tab = new string[size];
    for(int i = 0; i < size; i++){
        tab[i] = t[i];
    }
    objectNumber++;
}

tracer::tracer(const tracer& t): size(t.getSize()), printNumber(0){
    tab = new string[size];
    for(int i = 0; i < size; i++){
        tab[i] = t.tab[i];
    }
    objectNumber++;
}

tracer::~tracer(){
    delete [] tab;
    objectNumber--;
}

void tracer::concat(int k, int l){
    if(k < 0 || l < 0 || k >= size || l >= size){
        return;
    }
    tab[k] += tab[l];
}

string& tracer::operator[](int i) const{
    return tab[i];
}

int tracer::getSize() const{
    return size;
}

void tracer::print() const{
    printNumber++;
}

ostream& operator<<(ostream& out, const tracer& t){
    t.print();
    for(int i = 0; i < t.getSize(); i++){
        out << t[i] << endl;
    }
    return out;
}

int tracer::printno() const{
    return printNumber;
}

int tracer::objectno(){
    return objectNumber;
}

tracer& tracer::operator=(const tracer& t){
    if(&t != this){
        delete [] tab;
        size = t.getSize();
        tab = new string[size];
        for(int i = 0; i < size; i++){
            tab[i] = t.tab[i];
        }
    }
    return *this;
}
