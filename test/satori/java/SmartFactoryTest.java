package satori.java;

import org.junit.Test;
import satori.java.SmartFactory;

import java.util.Collection;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * @author Pawel Polit
 */
public class SmartFactoryTest {

    @Test
    public void test() {
        String ala = "Ala has a cat";
        Collection col = SmartFactory.fixIt(Collection.class, ala);
        assertEquals("Ala has a cat", col.toString());
        boolean ok = false;
        try {
            col.iterator();
        }
        catch(SmartFactory.HellNoException e) {
            ok = true;
        }
        assertTrue(ok);
    }
}
