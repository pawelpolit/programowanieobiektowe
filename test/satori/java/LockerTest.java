package satori.java;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;

/**
 * @author Pawel Polit
 */
public class LockerTest {
    private static List<String> result = new ArrayList<>();

    private static class FirstSillyThread extends Thread {

        Locker.Lock one, two;
        String me;
        int delay;

        public FirstSillyThread(String me, int delay, Locker.Lock one, Locker.Lock two) {
            this.me = me;
            this.delay = delay;
            this.one = one;
            this.two = two;
        }

        protected void log(String msg) {
            LockerTest.result.add("<" + me + ">: " + msg);
        }

        @Override
        public void run() {
            try {
                one.lock();
                log("got first lock");
                try {
                    TimeUnit.SECONDS.sleep(delay);
                    log("about to wait on second lock");
                    two.lock();
                    TimeUnit.MILLISECONDS.sleep(10);
                    log("got second lock");
                    two.unlock();
                } finally {
                    log("releasing first lock");
                    one.unlock();
                }
            } catch (Locker.DeadlockException e) {
                log("DeadlockException was thrown.");
            } catch (Exception s) {
                log("Other exception");
                s.printStackTrace();
            }
            log("finishing smoothly");
        }
    }

    public static class SecondSillyThread extends Thread {

        Locker.Lock one;
        String me;
        int delay;

        public SecondSillyThread(String me, int delay, Locker.Lock one) {
            this.me = me;
            this.delay = delay;
            this.one = one;
        }

        protected void log(String msg) {
            LockerTest.result.add("<" + me + ">: " + msg);
        }

        @Override
        public void run() {
            try {
                one.lock();
                log("got first lock");
                try {
                    TimeUnit.SECONDS.sleep(delay);
                    log("about to wait on second lock");
                    one.lock();
                    log("got second lock");
                    one.unlock();
                    log("unlocked second, time to sleep");
                    TimeUnit.SECONDS.sleep(delay);
                } finally {
                    log("releasing first lock");
                    one.unlock();
                }
            } catch (Locker.DeadlockException e) {
                log("DeadlockException was thrown.");
            } catch (Exception s) {
                log("Other exception");
            }
            log("finishing smoothly");
        }
    }

    @Test
    public void test1() throws InterruptedException {
        List<String> expectedResult = new ArrayList<>();
        expectedResult.add("<ONE>: got first lock");
        expectedResult.add("<TWO>: got first lock");
        expectedResult.add("<ONE>: about to wait on second lock");
        expectedResult.add("<TWO>: about to wait on second lock");
        expectedResult.add("<TWO>: releasing first lock");
        expectedResult.add("<TWO>: DeadlockException was thrown.");
        expectedResult.add("<TWO>: finishing smoothly");
        expectedResult.add("<ONE>: got second lock");
        expectedResult.add("<ONE>: releasing first lock");
        expectedResult.add("<ONE>: finishing smoothly");

        Locker locker = new Locker();
        Locker.Lock lock1 = locker.getLock(), lock2 = locker.getLock();
        new FirstSillyThread("ONE", 1, lock1, lock2).start();
        TimeUnit.MILLISECONDS.sleep(10);
        new FirstSillyThread("TWO", 2, lock2, lock1).start();

        Thread.sleep(2500);

        assertEquals(expectedResult, result);

        result.clear();
    }

    @Test
    public void test2() throws InterruptedException {
        List<String> expectedResult = new ArrayList<>();
        expectedResult.add("<ONE>: got first lock");
        expectedResult.add("<ONE>: about to wait on second lock");
        expectedResult.add("<ONE>: got second lock");
        expectedResult.add("<ONE>: releasing first lock");
        expectedResult.add("<ONE>: finishing smoothly");
        expectedResult.add("<TWO>: got first lock");
        expectedResult.add("<TWO>: about to wait on second lock");
        expectedResult.add("<TWO>: got second lock");
        expectedResult.add("<TWO>: releasing first lock");
        expectedResult.add("<TWO>: finishing smoothly");


        Locker locker = new Locker();
        Locker.Lock lock1 = locker.getLock(), lock2 = locker.getLock();
        new FirstSillyThread("ONE", 1, lock1, lock2).start();
        TimeUnit.MILLISECONDS.sleep(10);
        new FirstSillyThread("TWO", 2, lock1, lock2).start();

        Thread.sleep(3500);

        assertEquals(expectedResult, result);

        result.clear();
    }

    @Test
    public void test3() throws InterruptedException {
        List<String> expectedResult = new ArrayList<>();
        expectedResult.add("<ONE>: got first lock");
        expectedResult.add("<ONE>: about to wait on second lock");
        expectedResult.add("<ONE>: got second lock");
        expectedResult.add("<ONE>: unlocked second, time to sleep");
        expectedResult.add("<ONE>: releasing first lock");
        expectedResult.add("<ONE>: finishing smoothly");
        expectedResult.add("<TWO>: got first lock");
        expectedResult.add("<TWO>: about to wait on second lock");
        expectedResult.add("<TWO>: got second lock");
        expectedResult.add("<TWO>: unlocked second, time to sleep");
        expectedResult.add("<TWO>: releasing first lock");
        expectedResult.add("<TWO>: finishing smoothly");

        Locker locker = new Locker();
        Locker.Lock lock = locker.getLock();
        new SecondSillyThread("ONE", 1, lock).start();
        TimeUnit.MILLISECONDS.sleep(10);
        new SecondSillyThread("TWO", 1, lock).start();

        Thread.sleep(4500);

        assertEquals(expectedResult, result);

        result.clear();
    }
}
